<?php

$input = json_decode(file_get_contents('php://input'), true);
$array = array_values($input);

$con = include '../connect2_to_db.php';
// Set autocommit to off
mysqli_autocommit($con, FALSE);

for ($i = 0; $i < count($array); $i++) {
    $date = $array[$i]['date'];
    $hour = $array[$i]['hour'];
    $min = $array[$i]['min'];
    $x = $array[$i]['x'];
    $y = $array[$i]['y'];
    $z = $array[$i]['z'];

    $str = "INSERT INTO data ( 
        `date`,
        `hour`,
        `minute`,
        `x`,
        `y`,
        `z`
    )
    VALUES ('".
        $date."', '".
        $hour."', '".
        $min."', '".
        $x."', '".
        $y."', '".
        $z."'"
    .");";
    $res = mysqli_query($con, $str);
    if (!$res) {
        break;
    }
}
mysqli_commit($con);
print_r($i);
mysqli_close($con);