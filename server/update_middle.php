<?php

$input = json_decode(file_get_contents('php://input'), true);
$array = array_values($input);

$con = include '../connect2_to_db.php';
// Set autocommit to off
mysqli_autocommit($con, FALSE);
mysqli_query($con, "TRUNCATE TABLE middle;");
for ($i = 0; $i < count($array); $i++) {
    $date = $array[$i]['date'];
    $hour = $array[$i]['hour'];
    $min = $array[$i]['min'];
    $x = $array[$i]['x'];
    $y = $array[$i]['y'];
    $z = $array[$i]['z'];

    $str = "INSERT INTO middle ( 
        `date`,
        `hour`,
        `minute`,
        `x`,
        `y`,
        `z`
    )
    VALUES ('".
        $date."', '".
        $hour."', '".
        $min."', '".
        $x."', '".
        $y."', '".
        $z."'"
    .");";
    mysqli_query($con, $str);
}
mysqli_commit($con);
mysqli_close($con);